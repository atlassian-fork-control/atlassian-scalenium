package com.atlassian.scalenium.assertion

import org.scalatest.matchers.{MatchResult, Matcher}
import com.atlassian.pageobjects.elements.timeout.{TimeoutType, Timeouts}
import com.atlassian.pageobjects.elements.query._
import com.atlassian.pageobjects.util.InjectingTestedProducts
import org.scalatest.junit.{ShouldMatchersForJUnit, JUnitTestFailedError}
import com.atlassian.pageobjects.TestedProduct
import org.openqa.selenium.StaleElementReferenceException
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit.MILLISECONDS

/**
 * Provides the shouldEventually assertion for use with Atlassian Selenium.
 *
 * <pre class="stHighlight">
 * import junit.framework.TestCase
 * import com.atlassian.scalenium.assertion.TimedAssertions._
 *
 * class MyTestCase extends TestCase with TimedAssertions {
 *   val product = TestedProductFactory.create(classOf[JiraTestedProduct])
 *
 *   def testSomething() {
 *     myPageObject.getAjaxyThing shouldEventually be ("result of async call")
 *   }
 *
 *   // ...
 * }
 * </pre>
 *
 * @since 1.0
 */
trait TimedAssertions extends ShouldMatchersForJUnit {
  self: {def product: TestedProduct[_]} =>

  /**
   * Gets the Timeouts from the product.
   */
  private lazy val timeouts = InjectingTestedProducts.asInjectionContext(product).getInstance(classOf[Timeouts])

  /**
   * Wraps a Scala lazy function argument into a TimedQueryWrapper. This is what makes the magic possible.
   */
  implicit def convertToTimedQuery[T](query: => T): QueryShouldEventuallyWrapper[T] = new QueryShouldEventuallyWrapper[T](query)

  /**
   * Wrapper for a query (lazy function) that we wish to perform assertions on.
   */
  class QueryShouldEventuallyWrapper[T](query: => T) {
    /**
     * Implementation of "shouldEventually".
     *
     * @param rightMatcher a Matcher[T]
     * @param withTimeout how long to wait before giving up (default: TimeoutType.DEFAULT)
     */
    def shouldEventually(rightMatcher: Matcher[T], withTimeout: TimeoutType = TimeoutType.DEFAULT) {
      doShouldEventually(rightMatcher, Duration(Option(timeouts.timeoutFor(withTimeout)).getOrElse(5000L), MILLISECONDS))
    }

    /**
     * Implementation of "shouldEventually".
     *
     * @param rightMatcher a Matcher[T]
     * @param timeoutAfter how long to wait before giving up (e.g. "1 minute")
     */
    def shouldEventually(rightMatcher: Matcher[T], timeoutAfter: String) {
      doShouldEventually(rightMatcher, Duration.create(timeoutAfter))
    }

    /**
     * Implementation of "shouldEventually".
     *
     * @param rightMatcher a Matcher[T]
     * @param timeoutAfter how long to wait before giving up
     */
    private def doShouldEventually(rightMatcher: Matcher[T], timeoutAfter: Duration) {
      val timeoutMillis: Long = timeoutAfter.toMillis
      val matches = new ScalaTestMatcherCondition[T](query, rightMatcher, timeoutMillis)

      if (!matches.by(timeoutMillis))
      {
        matches.lastValue match
        {
          case Right(didNotMatch) => throw failedException(didNotMatch.failureMessage)
          case Left(staleEx) => throw staleEx // propagate the StaleElementReferenceException if the timeout has elapsed
        }
      }
    }
  }

  /**
   * Fails a JUnit test, fixing up the stack trace.
   */
  private def failedException(message: String, optionalCause: Option[Throwable] = None): Throwable = {
    val fileNames = List("TimedAssertions.scala")
    val temp = new RuntimeException
    val stackDepth = temp.getStackTrace.takeWhile(stackTraceElement => fileNames.exists(_ == stackTraceElement.getFileName) || stackTraceElement.getMethodName == "newTestFailedException").length

    optionalCause match {
      case Some(cause) => new JUnitTestFailedError(message, cause, stackDepth)
      case None => new JUnitTestFailedError(message, stackDepth)
    }
  }

  /**
   * Wrapper around a ScalaTest Matcher that performs "timed" evaluation of the query.
   */
  private class ScalaTestMatcherCondition[T](query: => T, rightMatcher: Matcher[T], timeoutMillis: Long) extends AbstractTimedCondition(timeoutMillis, timeouts.timeoutFor(TimeoutType.EVALUATION_INTERVAL)) {
    var lastValue: Either[StaleElementReferenceException, MatchResult] = _
    def currentValue() = {
      try {
        val matchResult: MatchResult = rightMatcher.apply(query)
        lastValue = Right(matchResult)
        matchResult.matches
      }
      catch {
        // treat a stale element exception as "no match"
        case e: StaleElementReferenceException => {
          lastValue = Left(e)
          false
        }
      }
    }
  }

}
